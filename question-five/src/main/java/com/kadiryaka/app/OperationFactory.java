package com.kadiryaka.app;

import com.kadiryaka.app.impl.DeleteOperationImpl;
import com.kadiryaka.app.impl.InsertOperationImpl;
import com.kadiryaka.app.impl.UpdateOperationImpl;

public class OperationFactory {
	public static Operation getInstance(ParameterDTO dto) {
		
		Operation operation = null;
		
		if (dto.getState().equals(State.INSERT)) {
			operation = new InsertOperationImpl();
		} else if (dto.getState().equals(State.DELETE)) {
			operation = new DeleteOperationImpl();
		} else if (dto.getState().equals(State.UPDATE)) {
			operation = new UpdateOperationImpl();
		}
		
		return operation;
	}
}