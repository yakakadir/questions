package com.kadiryaka.app;

public interface Operation {
	
	public Boolean doControl();

	public void makeOperation(ParameterDTO dto);
	
}