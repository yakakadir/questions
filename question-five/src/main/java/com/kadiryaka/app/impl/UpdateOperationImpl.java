package com.kadiryaka.app.impl;

import com.kadiryaka.app.Logger;
import com.kadiryaka.app.Operation;
import com.kadiryaka.app.ParameterDTO;

public class UpdateOperationImpl implements Operation {

	private ParameterDTO dto = null;
	
	public ParameterDTO getDto() {
		return dto;
	}

	public void setDto(ParameterDTO dto) {
		this.dto = dto;
	}
	
	public void update() {
		PersistenceImpl.parameters.put(getDto().getId(), getDto());
		Logger.log("dto updated in map");
	}

	@Override
	public Boolean doControl() {
Logger.log("dto will be checked for update process");
		
		if (PersistenceImpl.parameters.get(getDto().getId()) == null) {
			return false;
		}
		return true;
	}

	@Override
	public void makeOperation(ParameterDTO dto) {

		Logger.log("dto will be done operation for update process");
		
		this.dto = dto;
		
		if( doControl()) {
			update();
		}
	}
}