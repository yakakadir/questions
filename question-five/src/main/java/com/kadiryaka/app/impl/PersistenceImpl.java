package com.kadiryaka.app.impl;

import java.util.HashMap;
import java.util.Map;

import com.kadiryaka.app.ParameterDTO;

public class PersistenceImpl {
	
	static Map<Long, ParameterDTO> parameters = new HashMap<Long, ParameterDTO>();

	public ParameterDTO findById(long id) {
		return parameters.get(id);
	}
	
}