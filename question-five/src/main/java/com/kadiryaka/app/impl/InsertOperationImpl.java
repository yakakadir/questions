package com.kadiryaka.app.impl;

import com.kadiryaka.app.Logger;
import com.kadiryaka.app.Operation;
import com.kadiryaka.app.ParameterDTO;

public class InsertOperationImpl implements Operation {
	
	private ParameterDTO dto = null;
	
	public ParameterDTO getDto() {
		return dto;
	}

	public void setDto(ParameterDTO dto) {
		this.dto = dto;
	}
	
	public void insert() {
		PersistenceImpl.parameters.put(getDto().getId(), getDto());
		Logger.log("dto inserted to map");
	}
	
	@Override
	public Boolean doControl() {
		
		Logger.log("dto will be checked for insert process");
		
		if (PersistenceImpl.parameters.get(getDto().getId()) == null) {
			return true;
		}
		return false;
		
	}

	@Override
	public void makeOperation(ParameterDTO dto) {
		
		Logger.log("dto will be done operation for insert process");
		
		this.dto = dto;
		
		if( doControl()) {
			insert();
		}
		
	}


	
	
}