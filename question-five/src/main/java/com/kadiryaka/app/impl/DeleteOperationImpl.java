package com.kadiryaka.app.impl;

import com.kadiryaka.app.Logger;
import com.kadiryaka.app.Operation;
import com.kadiryaka.app.ParameterDTO;

public class DeleteOperationImpl implements Operation {
	
	private ParameterDTO dto = null;
	
	public ParameterDTO getDto() {
		return dto;
	}

	public void setDto(ParameterDTO dto) {
		this.dto = dto;
	}
	
	public void delete() {
		PersistenceImpl.parameters.remove(getDto().getId(), getDto());
		Logger.log("dto removed from map");
	}

	@Override
	public Boolean doControl() {
		
		Logger.log("dto will be checked for delete process");
		
		if (PersistenceImpl.parameters.get(getDto().getId()) == null) {
			return false;
		}
		return true;
		
	}
	@Override
	public void makeOperation(ParameterDTO dto) {
		
		Logger.log("dto will be done operation for delete process");
		
		this.dto = dto;
		
		if( doControl()) {
			delete();
		}
		
	}
}