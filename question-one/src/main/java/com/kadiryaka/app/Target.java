package com.kadiryaka.app;

public class Target {
	private long id;
	private String targetValue;
	private String key;
	private String testTarget;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTargetValue() {
		return targetValue;
	}

	public void setTargetValue(String targetValue) {
		this.targetValue = targetValue;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getTestTarget() {
		return testTarget;
	}

	public void setTestTarget(String testTarget) {
		this.testTarget = testTarget;
	}

	@Override
	public String toString() {
		return id + " " + targetValue + " " + key + " " + testTarget;
	}
}