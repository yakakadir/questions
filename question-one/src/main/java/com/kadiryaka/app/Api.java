package com.kadiryaka.app;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

public class Api {
	
	public static void populateTarget(Resource resource, Target target, ScenarioType eventType) throws Exception {
		
		//get all fields of classes
		Field[] resourceField = Resource.class.getDeclaredFields();
		Field[] targetField = Target.class.getDeclaredFields();
		
		Map<Integer, String[]> resourceMap = new HashMap<>();
		Map<Integer, String[]> targetMap = new HashMap<>();
		
		//add field type and field name in map
		for(int i=0; i<resourceField.length; i++) {
			resourceMap.put(i, new String[]{resourceField[i].getName(), resourceField[i].getType().getName()} );
		}
		
		//add field type and field name in map
		for(int i=0; i<targetField.length; i++) {
			targetMap.put(i, new String[]{targetField[i].getName(), targetField[i].getType().getName()} );
		}
		
		//map length control
		if (resourceMap.size() == targetMap.size()) {
			
			for(int i=0; i<resourceMap.size(); i++) {
				
				//type control
				if (resourceMap.get(i)[1].equals(targetMap.get(i)[1])) {
					try {
						//get target Field
						Field fieldTarget = target.getClass().getDeclaredField(targetMap.get(i)[0]);
						fieldTarget.setAccessible(true);
						
						//get resource Field
						Field fieldResource = resource.getClass().getDeclaredField(resourceMap.get(i)[0]);
						fieldResource.setAccessible(true);
						
						//set resources to target
						fieldTarget.set(target,fieldResource.get(resource));
						
						//close permission
						fieldTarget.setAccessible(false);
						fieldResource.setAccessible(false);
						
					} catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
						e.printStackTrace();
					}
					
				} else {
					if (eventType == ScenarioType.ADD_LOG) {
						System.out.println("ERROR " + "the field types of classes don't match");
					}
				}
			}
			
		} else {
			System.out.println("ERROR " + "the number of fields of classes don't match");
		}
	}

}
