package com.kadiryaka.app;

public class Application {

	public static void main(String[] args) throws Exception {
		
		 Resource resource = new Resource();
		 resource.setId(999);
		 resource.setKey(555L);
		 resource.setResourceValue("Resource");
		 resource.setTest("Test");
		 Target target = new Target();
		 System.out.println("Before call api");
		 System.out.println(resource);
		 System.out.println(target);
		 Api.populateTarget(resource, target, ScenarioType.ADD_LOG);
		 System.out.println("After call api");
		 System.out.println(resource);
		 System.out.println(target);
		 
	}

}
