package com.kadiryaka.app;

import java.lang.reflect.Field;

import org.junit.Assert;
import org.junit.Test;

public class ApiTest {
	
	@Test
	public void resourceReflactionFieldsArray_biggerThanZero() {
		Field[] resourceFields = Resource.class.getDeclaredFields();
		Assert.assertTrue("array length must bigger than 0", resourceFields.length > 0);
	}
	
	@Test
	public void targetReflactionFieldsArray_biggerThanZero() {
		Field[] targetFields = Target.class.getDeclaredFields();
		Assert.assertTrue("array length must bigger than 0", targetFields.length > 0);
	}
	
	@Test
	public void resourceAndTargetLength_beSame() {
		Field[] resourceFields = Resource.class.getDeclaredFields();
		Field[] targetFields = Target.class.getDeclaredFields();
		Assert.assertEquals(resourceFields.length, targetFields.length);
	}
	
	@Test
	public void resourceAndTargetFirstFieldType_beSame() {
		Field[] resourceFields = Resource.class.getDeclaredFields();
		Field[] targetFields = Target.class.getDeclaredFields();
		Assert.assertEquals(resourceFields[0].getType().getName(), targetFields[0].getType().getName());
	}
	
	@Test
	public void setResourceFieldValueToTargetField() {
		Target target = new Target();
		Resource resource = new Resource();
		resource.setId(100);
		
		Field fieldTarget = null;
		try {
			fieldTarget = target.getClass().getDeclaredField("id");
		} catch (NoSuchFieldException | SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		fieldTarget.setAccessible(true);
		
		Field fieldResource = null;
		try {
			fieldResource = resource.getClass().getDeclaredField("id");
		} catch (NoSuchFieldException | SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		fieldResource.setAccessible(true);
		
		try {
			fieldTarget.set(target,fieldResource.get(resource));
		} catch (IllegalArgumentException | IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Assert.assertEquals(target.getId(), resource.getId());
		
	}
}
