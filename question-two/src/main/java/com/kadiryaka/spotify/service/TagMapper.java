package com.kadiryaka.spotify.service;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.kadiryaka.spotify.entity.TrackEntity;

@Service
public class TagMapper {
	public Map<TrackEntity, List<String>> tracksTagMap = new HashMap<>();

	@Cacheable(value = "mycache")
	public Map<TrackEntity, List<String>> getTracksTagMap() {
		System.out.println("Executing getTracksTagMap method...");
		return tracksTagMap;
	}
	
	public Set<TrackEntity> getTrackentityListByTagName(String tagName) {
		
		    Set<TrackEntity> keys = new HashSet<>();
		    for (Entry<TrackEntity, List<String>> entry : tracksTagMap.entrySet()) {
		        if (entry.getValue().contains(tagName)) {
		            keys.add(entry.getKey());
		        }
		    }
		    return keys;
	}

}
