package com.kadiryaka.spotify.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.kadiryaka.spotify.dto.TrackResponseDTO;
import com.kadiryaka.spotify.entity.Item;
import com.kadiryaka.spotify.entity.SpotifySearchResult;
import com.kadiryaka.spotify.entity.TrackEntity;
import com.kadiryaka.spotify.service.TagMapper;

@Controller
public class TrackController {
	
	@Autowired
	TagMapper tagMapper;
	
	@RequestMapping(value="/home")
	public String goHomeAgain(Model model){
		
		return "track";
	}
	
	@RequestMapping(value="/search", method=RequestMethod.GET)
	public String search(@RequestParam("searchQuery") String searchQuery, Model model) {
		
		RestTemplate restTemplate = new RestTemplate();
		SpotifySearchResult result = null;
		List<TrackResponseDTO> resultList = new ArrayList<>();
		
		try {
			result = restTemplate.getForObject("https://api.spotify.com/v1/search?q=" + searchQuery + "&type=track", SpotifySearchResult.class);
		} catch (RestClientException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        for (Item item : result.getTracks().getItems()) {
			resultList.add(new TrackResponseDTO(item,tagMapper.getTracksTagMap()));
		}
        
        model.addAttribute("results", resultList);
        
        System.out.println();
        
		return "track";
	}
	
	@RequestMapping(value="/list/{tagName}", method=RequestMethod.GET)
	public String listTrackByTagName(@PathVariable("tagName") String tagName, Model model) {
		
		Set<TrackEntity> trackSet = tagMapper.getTrackentityListByTagName(tagName);
		List<TrackEntity> trackList = new ArrayList<>();
		trackList.addAll(trackSet);
		
		model.addAttribute("trackList", trackList);
        
		return "tracklist";
	}
	
	
}
