package com.kadiryaka.spotify.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kadiryaka.spotify.dto.TagRequestDTO;
import com.kadiryaka.spotify.entity.Tag;
import com.kadiryaka.spotify.entity.TrackEntity;
import com.kadiryaka.spotify.service.TagMapper;

@Controller
@RequestMapping("/tag")
public class TagController {

	@Value("${tag.number}")
    private String tagNumber;
	
	@Autowired
	TagMapper tagMapper;
	
	
	@RequestMapping(value="/add", method=RequestMethod.POST)
	public @ResponseBody String addTag(@RequestBody TagRequestDTO tagRequestDTO) {
		
		tagRequestDTO.getTrackEntity().setSinger(tagRequestDTO.getTrackEntity().getSinger().replaceAll("\\s+"," ").trim());
		
		List<String> list = tagMapper.getTracksTagMap().get(tagRequestDTO.getTrackEntity());
		if (list == null) {
			list = new ArrayList<>();
		}
		
		if (list.size() == Integer.valueOf(tagNumber)) {
			list.set(0,tagRequestDTO.getTagName());
		} else {
			list.add(tagRequestDTO.getTagName());
		}
		
		tagMapper.getTracksTagMap().put(tagRequestDTO.getTrackEntity(), list);
		
		return "OK";
	}

}
