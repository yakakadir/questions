package com.kadiryaka.spotify.dto;

import com.kadiryaka.spotify.entity.TrackEntity;

public class TagRequestDTO {

	TrackEntity trackEntity;
	
	String tagName;

	public TrackEntity getTrackEntity() {
		return trackEntity;
	}

	public void setTrackEntity(TrackEntity trackEntity) {
		this.trackEntity = trackEntity;
	}

	public String getTagName() {
		return tagName;
	}

	public void setTagName(String tagName) {
		this.tagName = tagName;
	}
	
	
	
}
