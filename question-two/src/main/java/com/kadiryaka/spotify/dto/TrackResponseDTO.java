package com.kadiryaka.spotify.dto;

import java.util.List;
import java.util.Map;

import com.kadiryaka.spotify.entity.Artist;
import com.kadiryaka.spotify.entity.Artist_;
import com.kadiryaka.spotify.entity.Item;
import com.kadiryaka.spotify.entity.Tag;
import com.kadiryaka.spotify.entity.TrackEntity;

public class TrackResponseDTO {
	
	private Item item;
	private List<String> tagList;
	
	public TrackResponseDTO() {
		
	}
	
	public TrackResponseDTO (Item item, Map<TrackEntity, List<String>> map) {
		
		this.item = item;
		this.tagList = seperateTag(item, map);
		
	}
	
	public List<String> seperateTag(Item item, Map<TrackEntity, List<String>> map) {
		List<String> tagList = null;
		StringBuilder artists = new StringBuilder();

		
		for (Artist_ artist : item.getArtists()) {
			artists.append(artist.getName() + " ");
		}
		
		TrackEntity trackEntity = new TrackEntity(item.getId(), item.getName(), artists.toString().replaceAll("\\s+"," ").trim());
		
		tagList = map.get(trackEntity);
		
		return tagList;
	}
	
	public Item getItem() {
		return item;
	}
	public void setItem(Item item) {
		this.item = item;
	}
	public List<String> getTagList() {
		return tagList;
	}
	public void setTagList(List<String> tagList) {
		this.tagList = tagList;
	}


}
