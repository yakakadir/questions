$(document).ready(function(){
    
	$(".addTagButton").click(function(){
        var trackId = this.id;
        var tagName = $("#input"+trackId).val();
        var trackName = $("#sing"+trackId).text();
        var trackArtists = $("#artist"+trackId).text();
        
        var trackEntity = {
        		 "id" : trackId,
        		 "name" : trackName,
        		 "singer" : trackArtists,
        }
        
        //$("#tag"+trackId).append("<a href='/spotify-api/list/"+tagName+"'>"   + tagName + "</a>&nbsp;");
        
        $.ajax({
        	   url: '/spotify-api/tag/add',
        	   data : JSON.stringify({
        		  "trackEntity" : trackEntity,
        	      "tagName" : tagName
        	   }),
        	   contentType : 'application/json',
        	   error: function(error) {
        		   console.log(error);
        	   },
        	   success: function(data) {
        		   location.reload();
        	   },
        	   type: 'POST'
        	});
    });
	
});