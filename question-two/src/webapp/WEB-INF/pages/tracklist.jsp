<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="<c:url value="/js/basejs.js" />"></script>
<title>Spotify</title>
</head>
<body>


	<table>
		<th>Şarkıcı</th>
		<th>Şarkı adı</th>
		
		<c:forEach items="${trackList}" var="result">

	         <tr>
		         
		         <td>
		         	<label>${result.singer}</label>
		         </td>
		         
		         <td><label>${result.name}</label></td>
	         
	        </tr>
    	</c:forEach>
	</table>
	
	
	
</body>
</html>