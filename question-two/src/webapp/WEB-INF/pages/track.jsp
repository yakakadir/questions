<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="<c:url value="/js/basejs.js" />"></script>
<title>Spotify</title>
</head>
<body>
	
<spring:url var="searchActionUrl" value="/search"></spring:url>

	<br/><br/><br/>

	<form method="GET" action="${searchActionUrl}">
		Şarkı veya şarkıcı ara : <input name="searchQuery" type="text" />
	</form>
	
	<br/><br/><br/>
	
	<table>
		<th>Şarkıcı</th>
		<th>Şarkı adı</th>
		<th>Tagler</th>
		<th>Tag Ekle</th>
		<c:forEach items="${results}" var="result">

	         <tr>
	         
	         <td>
	         	<label id="artist${result.item.id}">
			         <c:forEach items="${result.item.artists}" var="artist">
				        ${artist.name}
		    		</c:forEach>
	    		</label>
	         </td>
	         
	         <td><label id="sing${result.item.id}" >${result.item.name}</label></td>
	         
	          <td>
				<label id="tag${result.item.id}" >
			        <c:forEach items="${result.tagList}" var="tag">
				        <a href="/spotify-api/list/${tag}">${tag}</a>&nbsp; 
		    		</c:forEach>
	    		</label>
	    		
	         </td>
	         
	         <td>
	         	<input id="input${result.item.id}" name="searchQuery" type="text" /> <button class="addTagButton" id="${result.item.id}">Ekle</button>
	         </td>
	            
	        </tr>
    	</c:forEach>
	</table>
	
	
	
</body>
</html>