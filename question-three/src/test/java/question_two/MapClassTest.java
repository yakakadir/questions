package question_two;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;

import com.kadiryaka.app.MapClass;

public class MapClassTest {
	
	
	@Test
	public void keyList_equalKeyValue() {
		MapClass<String, String, String> map = getMockMap();
		
		List<String> childrenKeyList = new ArrayList<>();
		
		for (Map.Entry<String, HashMap<String, String>> entry : map.entrySet()) {
			childrenKeyList.add(entry.getKey());
		}
		
		Assert.assertEquals(childrenKeyList.get(0), "key1");
		Assert.assertEquals(childrenKeyList.get(1), "key3");
		Assert.assertTrue("list size must 2", childrenKeyList.size() == 2);
		
	}
	
	@Test
	public void valueList_equalKeyValue() {
		MapClass<String, String, String> map = getMockMap();
		
		List<String> childrenValueList = new ArrayList<>();
		
		for (Map.Entry<String, HashMap<String, String>> entry : map.entrySet()) {
			for(Map.Entry<String, String> childEntry:entry.getValue().entrySet()) {
				childrenValueList.add(childEntry.getValue());
			}
		}
		
		Assert.assertEquals(childrenValueList.get(0), "value1");
		Assert.assertEquals(childrenValueList.get(1), "value2");
		Assert.assertTrue("list size must 2", childrenValueList.size() == 2);
		
	}
	
	@Test
	public void childrenValue_equalKey() {
		MapClass<String, String, String> map = getMockMap();
		Assert.assertEquals(map.getChildrenValue("key1", "key2"),"value1");
	}
	
	@Test
	public void removeValueAtChildren_equalNull() {
		MapClass<String, String, String> map = getMockMap();
		
		HashMap<String, String> childMap = map.get("key1");
	    if (childMap == null) { 
	      childMap = new HashMap<String, String>();
	      map.put("key1", childMap);
	    }
	    childMap.put("key2", null);
		
		Assert.assertEquals(map.getChildrenValue("key1", "key2"), null);
	}
	
	
	public MapClass<String, String, String> getMockMap() {
		MapClass<String, String, String> map = new MapClass<>();
		
		map.putChildren("key1", "key2", "value1");
		map.putChildren("key3", "key4", "value2");
		
		return map;
		
	}

}
