package com.kadiryaka.app;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MapClass<T,K,U> extends HashMap<T, HashMap<K, U>> {

	private static final long serialVersionUID = 1L;
	
	public MapClass() {
		super();
	}
	
	public MapClass(Map<? extends T, ? extends U> m) {
		super();
	}
	
	public void putChildren(T key1, K key2, U value) {
		HashMap<K, U> childMap = get(key1);
	      if (childMap == null) { 
	        childMap = new HashMap<K, U>();
	        put(key1, childMap);
	      }
	      childMap.put(key2, value);
	}
	
	public List<T> listChildrenKeys() {
		List<T> childrenKeyList = new ArrayList<>();
		
		for (Map.Entry<T, HashMap<K, U>> entry : this.entrySet()) {
			childrenKeyList.add(entry.getKey());
		}
		
		return childrenKeyList;
	}
	
	public List<U> listChildrenValues() {
		List<U> childrenValueList = new ArrayList<>();
		
		for (Map.Entry<T, HashMap<K, U>> entry : this.entrySet()) {
			for(Map.Entry<K, U> childEntry:entry.getValue().entrySet()) {
				childrenValueList.add(childEntry.getValue());
			}
		}
		
		return childrenValueList;
	}
	
	public U getChildrenValue(T key, K key2) {
		HashMap<K,U> map =  this.get(key);
		return map.get(key2);
	}
	
	public void removeValueAtChildren(T key1, K key2) {
		HashMap<K, U> childMap = get(key1);
		if (childMap == null) { 
		  childMap = new HashMap<K, U>();
		  put(key1, childMap);
		}
		childMap.put(key2, null);
	}

}
