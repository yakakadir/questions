package com.kadiryaka.app;

import java.util.List;

public class Application {

	public static void main(String[] args) {
		
		MapClass<String, String, String> childrenMap = new MapClass<>();
		childrenMap.putChildren("kadir", "yaka", "Turkninja");
		childrenMap.putChildren("recep", "yakar", "türkninja2");
		
		List<String> listChildrenKeys = childrenMap.listChildrenKeys();
		for (String string : listChildrenKeys) {
			System.out.println(string);
		}
		
		List<String> listChildrenValues = childrenMap.listChildrenValues();
		for (String string : listChildrenValues) {
			System.out.println(string);
		}
		
		System.out.println(childrenMap.getChildrenValue("kadir", "yaka"));
		
		childrenMap.removeValueAtChildren("kadir", "yaka");
		System.out.println(childrenMap.getChildrenValue("kadir", "yaka"));
		

	}

}
